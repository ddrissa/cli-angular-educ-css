import { CliAngular2EducCssPage } from './app.po';

describe('cli-angular2-educ-css App', () => {
  let page: CliAngular2EducCssPage;

  beforeEach(() => {
    page = new CliAngular2EducCssPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
